import model.Invoiceltem;

public class App {
    public static void main(String[] args) throws Exception {
        Invoiceltem invoiceltem1 = new Invoiceltem(1, "Tivi", 100, 32.500000);
        Invoiceltem invoiceltem2 = new Invoiceltem(2, "SmartPhone", 100, 24.450000);
        System.out.println("invoiceltem1");
        System.out.println(invoiceltem1.toString());
        System.out.println(invoiceltem1.getTotal());
        System.out.println(invoiceltem1);
        System.out.println("invoiceltem2");
        System.out.println(invoiceltem2.toString());
        System.out.println(invoiceltem2.getTotal());
        System.out.println(invoiceltem2);

    }
}
